#!/usr/bin/python3

import argparse
import pathlib

STATE_PATH = pathlib.Path("/var/lib/altman/state")
SRC_PATH = pathlib.Path("/usr/share/alternatives")

def getItemStatePath(item_name):
    return STATE_PATH / item_name
def getItemSrcPath(item_name):
    return SRC_PATH / item_name

def readItemState(item_name):
    path = getItemStatePath(item_name)

    with open(path, 'r') as h:
        lines = h.readlines()

        choice = lines.pop(0).strip()

        links = []
        for line in lines:
            value = line.strip()
            if len(value) > 0:
                links.append(value)

        return {"choice": choice, "links": links}

def writeItemState(item_name, state):
    path = getItemStatePath(item_name)

    if state["choice"] == None:
        if len(state["links"]) > 0:
            raise RuntimeError("Unable to save state with no choice")
        path.unlink()
        return

    path.parent.mkdir(parents=True, exist_ok=True)

    with open(path, 'w') as h:
        h.write(state["choice"] + "\n\n")
        for link in state["links"]:
            h.write(link + "\n")

def resolveItemState(item_name, choose_if_missing, new_choice):
    if new_choice != None:
        item_src_path = getItemSrcPath(item_name)

        if not (item_src_path / new_choice).exists():
            raise RuntimeError("Specified choice (%s) could not be found for item (%s)" % (new_choice, item_name))

        prev_state = None
        try:
            prev_state = readItemState(item_name)
        except FileNotFoundError:
            pass

        new_state = None
        if prev_state != None:
            new_state = {"choice": new_choice, "links": prev_state["links"]}
        else:
            new_state = {"choice": new_choice, "links": []}

        return new_state

    choice_removed = False
    existing_links = []

    try:
        state = readItemState(item_name)
        if not (getItemSrcPath(item_name) / state["choice"]).exists():
            choice_removed = True
            existing_links = state["links"]
            raise FileNotFoundError()
        return state
    except FileNotFoundError:
        if not choose_if_missing:
            raise

        item_src_path = getItemSrcPath(item_name)

        first = None
        try:
            first = next(item_src_path.iterdir())
        except FileNotFoundError:
            pass

        if first == None:
            if len(existing_links) > 0:
                print("No alternatives remaining for %s, removing links" % (item_name))
            return {"choice": None, "links": existing_links}

        choice = first.name

        if choice_removed:
            print("Selected alternative for %s is missing, choosing %s" % (item_name, choice))
        else:
            print("No alternative set for %s, choosing %s" % (item_name, choice))

        new_state = {"choice": choice, "links": existing_links}

        return new_state

def parseChoiceFile(line):
    spl = line.split("\t")
    if len(spl) != 2:
        raise ValueError("Invalid number of columns in choice description: Expected 2, found " + str(len(spl)))
    return {"src": spl[1], "dest": spl[0]}

def readChoiceConf(item_name, choice):
    path = getItemSrcPath(item_name) / choice

    links = []

    with open(path, 'r') as h:
        lines = h.readlines()
        for line in lines:
            line = line.strip()
            if len(line) > 0:
                mapping = parseChoiceFile(line)
                links.append(mapping)
    return {"links": links}

def updateItem(item_name, newChoice):
    current_state = resolveItemState(item_name, True, newChoice)

    choice = current_state["choice"]

    choice_info = None
    if choice != None:
        choice_info = readChoiceConf(item_name, choice)

    link_changes = {}

    for link in current_state["links"]:
        link_changes[link] = {"action": "delete"}

    if choice_info != None:
        for link in choice_info["links"]:
            src = pathlib.Path(link["src"])
            dest = pathlib.Path(link["dest"])

            key = str(dest)

            if key in link_changes:
                current_action = link_changes[key]
                if current_action["action"] == "delete":
                    current_action["action"] = "change"
                    current_action["target"] = src
                else:
                    raise RuntimeError("Multiple targets specified for path %s" % (key,))
            else:
                link_changes[key] = {"action": "create", "target": src}

    new_state = {"choice": choice, "links": []}

    for key, action in link_changes.items():
        dest = pathlib.Path(key)
        if action["action"] == "create" or action["action"] == "change":
            new_state["links"].append(key)

        if action["action"] == "delete" or action["action"] == "change":
            current_target = dest.resolve()
            if action["action"] == "change" and current_target == action["target"].resolve():
                continue # already linked
            try:
                dest.unlink()
                print("Unlinking: %s -> %s" % (dest, current_target))
            except FileNotFoundError:
                pass

        if action["action"] == "create":
            dest.parent.mkdir(parents=True, exist_ok=True)

        if action["action"] == "create" or action["action"] == "change":
            src = action["target"]
            print("Linking: %s -> %s" % (dest, src))
            dest.symlink_to(src)

    writeItemState(item_name, new_state)

parser = argparse.ArgumentParser()
parser.add_argument("item", metavar="item", type=str, nargs='*')
parser.add_argument("--choose", type=str)
parser.add_argument("--state-dir", type=pathlib.Path, default=STATE_PATH)
parser.add_argument("--src-dir", type=pathlib.Path, default=SRC_PATH)
args = parser.parse_args()

STATE_PATH = args.state_dir
SRC_PATH = args.src_dir

items = None
if len(args.item) > 0:
    items = args.item
else:
    if args.choose != None:
        raise RuntimeError("--choose cannot be specified without an item")
    items = set()
    try:
        items = items | {item.name for item in SRC_PATH.iterdir()}
    except FileNotFoundError:
        pass
    try:
        items = items | {item.name for item in STATE_PATH.iterdir()}
    except FileNotFoundError:
        pass

for item in items:
    updateItem(item, args.choose)
